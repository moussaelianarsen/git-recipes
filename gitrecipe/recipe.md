1. Go to the [recipes repo](https://gitlab.com/moussaelianarsen/git-recipes)
2. Fork the repository
3. Add a recipe like this one
4. Create a merge request
5. Wait up to 30 minutes for sync
